package com.company;

import java.util.*;

public class Petshop {
    private Map<String,Pessoa> clientes;

    public Petshop() {
        clientes = new LinkedHashMap<>();
    }

    public void addCliente(Pessoa cliente){
        clientes.put(cliente.getCpf(), cliente);
    }
    public void removeCliente(String cpf){
        clientes.remove(cpf);
    }

    public List<Pessoa> getClientes(){
        Collection<Pessoa> result = clientes.values();
        return new ArrayList<>(result);
    }

    public Pessoa getCliente(String cpf){
        return clientes.get(cpf);
    }


}
