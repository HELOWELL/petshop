package com.company;

import java.util.List;
import java.util.Scanner;

public class Main {

    static Petshop petshop = new Petshop();
    public static void main(String[] args) {
       int opcao = -1;
       String nome = "helo";
       String cpf ="000.000.000-00";
       //String nomeCachorro = "dobby";
       //String raca = "pug";
       //boolean vacinado = true;
        do{
            opcao = getOpcao();
           switch (opcao){
               case 1:
                   addPessoa(nome, cpf);
                   break;
               case 2:
                   addCachorro();
                   break;
               case 3:
                   listCachorros();
                   break;
               case 4:
                   removePessoa();
                   break;
               case 5:
                   listPessoa(cpf);
               case 6:
                   removeCachorro();
                   break;
               case 7:
                   listVacinados();
                   break;
           }
       }while(opcao != 0);

    }

    private static void listVacinados() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entre com o cpf do cliente");
        String cpf = scanner.nextLine();
        Pessoa cliente = petshop.getCliente(cpf);
        List<Cachorro> dogs = cliente.getDogs();
        for (Cachorro dog : dogs) {
            System.out.println(dog.getName() + " Vacina: " + dog.isVacinado());

        }
    }

    private static void removeCachorro() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entre com o cpf do cliente");
        String cpf = scanner.nextLine();
        System.out.println("Entre com o nome do Cachorro");
        String nomeCachorro = scanner.nextLine();
        System.out.println("Entre com a raça do Cachorro");
        String racaCachorro = scanner.nextLine();
        System.out.println("Escreva true se o cão for vacinado ou false");
        String temvacina = scanner.nextLine();
        Boolean vacina = Boolean.valueOf(temvacina);
        Cachorro cachorro = new Cachorro(nomeCachorro, racaCachorro, vacina);
        Pessoa cliente = petshop.getCliente(cpf);
        cliente.removeDoguinho(cachorro);
    }

    private static void listPessoa(String cpf) {
        List<Pessoa> clientes = petshop.getClientes();
        System.out.println(clientes);
    }

    private static void removePessoa() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Entre com o nome do cliente");
        String nomePessoa = scanner.nextLine();
        System.out.println("Entre com o cpf do cliente");
        String cpfPessoa = scanner.nextLine();
        Pessoa pessoa = new Pessoa(nomePessoa, cpfPessoa);
        petshop.removeCliente(cpfPessoa);
    }

    private static int getOpcao() {
        Scanner scanner =  new Scanner(System.in);
        System.out.println("1 - Adiciona pessoa");
        System.out.println("2 - Adiciona cachorro");
        System.out.println("3 - Lista cachorros (cpf)");
        System.out.println("4 - Remove pessoa (CPF)");
        System.out.println("5 - Lista pessoa (cpf)");
        System.out.println("6 - Remove Cachorro");
        System.out.println("7 - Lista de cachorros vacinados");
        System.out.print("Digite uma opção: ");
        int escolha = Integer.parseInt(scanner.nextLine());
        return escolha;
    }

    private static void listCachorros() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Digite o cpf do cliente");
        String cpf = scanner.nextLine();
        Pessoa cliente = petshop.getCliente(cpf);
        List<Cachorro> dogs = cliente.getDogs();
        for (Cachorro dog : dogs) {
            System.out.println(dog.getName());
        }
    }


    private static void addCachorro() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite o cpf do cliente");
        String cpf = scanner.nextLine();
        System.out.println("Escreva o nome do cachorro");
        String nomeDog = scanner.nextLine();
        System.out.println("Escreva a raça do cachorro");
        String racaDog = scanner.nextLine();
        System.out.println("Escreva true se o cão for vacinado ou false");
        String temvacina = scanner.nextLine();
        Boolean vacina = Boolean.valueOf(temvacina);
        Cachorro cachorro = new Cachorro(nomeDog, racaDog, vacina);

        Pessoa cliente = petshop.getCliente(cpf);
        //Cachorro cachorro = new Cachorro(nomeCachorro, raca, vacinado);
        cliente.addDoguinho(cachorro);
    }

    private static void addPessoa(String nome, String cpf) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Escreva o nome do cliente");
        String nomePessoa = scanner.nextLine();
        System.out.println("Escreva o cpf do cliente");
        String cpfPessoa = scanner.nextLine();
        Pessoa pessoa = new Pessoa(nomePessoa, cpfPessoa);
        petshop.addCliente(pessoa);
    }


}
