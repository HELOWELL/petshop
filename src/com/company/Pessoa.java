package com.company;

import java.util.ArrayList;
import java.util.List;

public class Pessoa {
    private final List<Cachorro> dogs;
    private String nome;
    private String cpf;

    public Pessoa(String nome, String cpf) {
        this.nome = nome;
        this.cpf = cpf;
        this.dogs = new ArrayList<>();
    }

    public String getNome() {
        return nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void addDoguinho(Cachorro cachorro){
        dogs.add(cachorro);
    }

    public void removeDoguinho(Cachorro cachorro){
        dogs.remove(cachorro);
    }

    public List<Cachorro> getDogs() {
        return new ArrayList<>(dogs);
    }

    public List<Cachorro> getDogsVacinados(boolean isVacinado) {
        List<Cachorro> resultado = new ArrayList<>();
        for (Cachorro dog : dogs) {
            if(dog.isVacinado() == isVacinado){
                resultado.add(dog);
            }
        }
        return resultado;
    }

    @Override
    public String toString() {
        return "Pessoa{" +
                "dogs=" + dogs +
                ", nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                '}';
    }
}
