package com.company;

import java.util.Objects;

public class Cachorro {
    private String name;
    private String raça;
    private boolean vacinado;

    public Cachorro(String name, String raça, boolean vacinado) {
        this.name = name;
        this.raça = raça;
        this.vacinado = vacinado;
    }

    public String getName() {
        return name;
    }

    public String getRaça() {
        return raça;
    }

    public boolean isVacinado() {
        return vacinado;
    }

    public void setVacinado(boolean vacinado) {
        this.vacinado = vacinado;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cachorro cachorro = (Cachorro) o;
        return Objects.equals(name, cachorro.name) && Objects.equals(raça, cachorro.raça);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, raça);
    }

    @Override
    public String toString() {
        return "Cachorro{" +
                "name='" + name + '\'' +
                ", raça='" + raça + '\'' +
                ", vacinado=" + vacinado +
                '}';
    }
}
